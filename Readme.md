# Getting Started

## Reference Documentation
__Please start service__
1. registry-service
2. config-service
3. master-data-service
4. user-service

For further reference, please consider the following sections:

* SQL Script
* Swagger UI
* Postman

Copyright (c) 2021 MB Shafiq Test Code / Single Service Spring-boot App
### 1. SQL Script
> CREATE TABLE `account` (
  `id` varchar(255) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  `active` bit(1) NOT NULL DEFAULT b'1',
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile_no` varchar(45) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `task` varchar(45) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



### 2. Swagger UI
You may use swagger ui here 
`http://localhost:8882/swagger-ui.html` to play around with the APIS here instead.



### 3. Postman Collections
Please find the collections inside folder
`fiq-user-service/postman` to play around with the APIS using postman.