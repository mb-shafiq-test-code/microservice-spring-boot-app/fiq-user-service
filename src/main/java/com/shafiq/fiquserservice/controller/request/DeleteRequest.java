package com.shafiq.fiquserservice.controller.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shafiq.fiquserservice.services.FieldAttributes;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeleteRequest {

    @FieldAttributes(nullable = false, isEmail = true, max = 50)
    @JsonProperty("email")
    private String email;
}
