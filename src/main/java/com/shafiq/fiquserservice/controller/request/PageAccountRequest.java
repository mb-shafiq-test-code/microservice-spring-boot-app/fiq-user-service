package com.shafiq.fiquserservice.controller.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shafiq.fiquserservice.services.FieldAttributes;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageAccountRequest {

    @FieldAttributes(allowableValues = { "","Admin", "Clerk", "Staff","Or Empty String"})
    @JsonProperty("task")
    private String task;

    @FieldAttributes(nullable = false, minValue = 0)
    @JsonProperty("page")
    private Integer page;

    @FieldAttributes(nullable = false, minValue = 1)
    @JsonProperty("size")
    private Integer size;
}
