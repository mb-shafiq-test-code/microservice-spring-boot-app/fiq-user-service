package com.shafiq.fiquserservice.controller.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shafiq.fiquserservice.services.FieldAttributes;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountRequest {

    @FieldAttributes(nullable = false, max = 255)
    @JsonProperty("fullName")
    private String fullName;

    @FieldAttributes(nullable = false, isEmail = true, max = 50)
    @JsonProperty("email")
    private String email;

    @FieldAttributes(nullable = false, max = 15)
    @JsonProperty("mobileNo")
    private String mobileNo;

    @JsonProperty("birthDate")
    private Date birthDate;

    @FieldAttributes(nullable = false, max = 10, allowableValues = {"Admin", "Clerk", "Staff"})
    @JsonProperty("task")
    private String task;
}
