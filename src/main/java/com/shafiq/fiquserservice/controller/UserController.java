package com.shafiq.fiquserservice.controller;

import com.shafiq.fiquserservice.controller.request.AccountRequest;
import com.shafiq.fiquserservice.controller.request.DeleteRequest;
import com.shafiq.fiquserservice.controller.request.PageAccountRequest;
import com.shafiq.fiquserservice.controller.response.ResponseInfo;
import com.shafiq.fiquserservice.services.FieldAttributesValidator;
import com.shafiq.fiquserservice.services.LogService;
import com.shafiq.fiquserservice.services.UserService;
import com.shafiq.fiquserservice.utils.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@Slf4j
public class UserController {

    public final String reqRawText = " [Request Raw] : ";
    public final String resRawText = " [Response Raw] : ";
    @Autowired
    LogService logService;
    @Autowired
    UserService userService;
    ResponseInfo responseInfo;

    @PostMapping("/user/account/add")
    public Object addAccount(@RequestBody AccountRequest request) {
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + request);

        responseInfo = FieldAttributesValidator.validateRequest(request);

        if (!Objects.isNull(responseInfo)) {
            logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                    resRawText + responseInfo);
            return responseInfo;
        }

        responseInfo = userService.saveUserAccount(request);
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }

    @PostMapping("/user/account/edit")
    public Object editAccount(@RequestBody AccountRequest request) {
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + request);

        responseInfo = FieldAttributesValidator.validateRequest(request);

        if (!Objects.isNull(responseInfo)) {
            logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                    resRawText + responseInfo);
            return responseInfo;
        }

        responseInfo = userService.updateUserAccount(request);
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }

    @PostMapping("/user/account/delete")
    public Object deleteAccount(@RequestBody DeleteRequest request,
                                @RequestParam(defaultValue = "false") Boolean hardDelete) {
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + request + ", hardDelete=" + hardDelete);

        responseInfo = FieldAttributesValidator.validateRequest(request);

        if (!Objects.isNull(responseInfo)) {
            logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                    resRawText + responseInfo);
            return responseInfo;
        }

        responseInfo = userService.deleteUserAccount(request, hardDelete);
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }

    @GetMapping("/user/account/search")
    public Object searchAccount(@RequestParam(defaultValue = "") String email) {
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + "email=" + email);

        if (BaseUtil.getStr(email).equals("")) {
            responseInfo = ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("101")
                    .responseMessage("Failed due to email parameter cannot be empty")
                    .build();
            logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                    resRawText + responseInfo);
            return responseInfo;
        }

        responseInfo = userService.readUserAccount(email);
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }

    @GetMapping("/user/account/page")
    public Object listPageAccount(@RequestParam(defaultValue = "0") Integer page,
                                  @RequestParam(defaultValue = "10") Integer size,
                                  @RequestParam(required = false) String task) {
        PageAccountRequest request = PageAccountRequest.builder()
                .page(page)
                .size(size)
                .task(task)
                .build();

        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + request);

        responseInfo = FieldAttributesValidator.validateRequest(request);

        if (!Objects.isNull(responseInfo)) {
            logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                    resRawText + responseInfo);
            return responseInfo;
        }

        responseInfo = userService.getUserAccountList(request);
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }
}
