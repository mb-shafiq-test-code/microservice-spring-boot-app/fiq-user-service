package com.shafiq.fiquserservice.controller.handler;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.shafiq.fiquserservice.services.LogService;
import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    LogService logService;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ResponseInfo resp = ResponseInfo.builder()
                .responseStatus("F")
                .responseCode("100")
                .responseMessage(e.getClass().getName())
                .responseDetail(e.getMessage())
                .build();
        logService.saveLogError(e.getClass().getName(), resp.toString());
        return new ResponseEntity<>(resp, status);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<ResponseInfo> handleConflict(RuntimeException e, WebRequest request) {
        ResponseInfo resp = ResponseInfo.builder()
                .responseStatus("F")
                .responseCode("101")
                .responseMessage("This should be application specific")
                .responseDetail(e.getMessage())
                .build();
        logService.saveLogError(e.getClass().getName(), resp.toString());
        return new ResponseEntity<>(resp, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value={ConversionFailedException.class})
    public ResponseEntity<ResponseInfo> handleConversionConflict(RuntimeException e, WebRequest request) {
        ResponseInfo resp = ResponseInfo.builder()
                .responseStatus("F")
                .responseCode("102")
                .responseMessage("Conversion Failed Exception")
                .responseDetail(e.getMessage())
                .build();
        logService.saveLogError(e.getClass().getName(), resp.toString());
        return new ResponseEntity<>(resp, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ResponseInfo> handleSQLConflict(RuntimeException e, WebRequest request) {
        ResponseInfo resp = ResponseInfo.builder()
                .responseStatus("F")
                .responseCode("103")
                .responseMessage("Seem data (Email) already exist")
                .responseDetail(e.getMessage())
                .build();
        logService.saveLogError(e.getClass().getName(), resp.toString());
        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseInfo> handleGlobalConflict(Exception e) {
        ResponseInfo resp = ResponseInfo.builder()
                .responseStatus("F")
                .responseCode("104")
                .responseMessage(e.getClass().getName())
                .responseDetail(e.getMessage())
                .build();
        logService.saveLogError(e.getClass().getName(), resp.toString());
        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @Builder
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ResponseInfo {
        private String responseStatus;
        private String responseCode;
        private String responseMessage;
        private Object responseDetail;
    }


}
