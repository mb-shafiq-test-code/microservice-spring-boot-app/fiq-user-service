package com.shafiq.fiquserservice.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExtUserResponse {
    @JsonProperty("id")
    public int id;
    @JsonProperty("name")
    public String name;
    @JsonProperty("username")
    public String username;
    @JsonProperty("email")
    public String email;
    @JsonProperty("address")
    public Address address;
    @JsonProperty("phone")
    public String phone;
    @JsonProperty("website")
    public String website;
    @JsonProperty("company")
    public Company company;

    @Builder
    @Data
    public static class Address{
        public String street;
        public String suite;
        public String city;
        public String zipcode;
        public Geo geo;

        @Builder
        @Data
        public static class Geo{
            public String lat;
            public String lng;
        }
    }

    @Builder
    @Data
    public static class Company{
        public String name;
        public String catchPhrase;
        public String bs;
    }
}
