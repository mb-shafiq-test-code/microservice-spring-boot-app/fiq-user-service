package com.shafiq.fiquserservice.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseInfo {
    private String responseStatus;
    private String responseCode;
    private String responseMessage;
    private Object responseDetail;
}
