package com.shafiq.fiquserservice.controller;

import com.shafiq.fiquserservice.controller.response.ResponseInfo;
import com.shafiq.fiquserservice.services.ExternalService;
import com.shafiq.fiquserservice.services.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ExternalController {

    public final String reqRawText = " [Request Raw] : ";
    public final String resRawText = " [Response Raw] : ";
    @Autowired
    LogService logService;
    @Autowired
    ExternalService externalService;
    ResponseInfo responseInfo;

    @GetMapping("/external/getpost")
    public Object getExternalApisPost() {
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + "getPost");

        responseInfo = externalService.getExternalAllPost();

        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }

    @GetMapping("/external/getuser")
    public Object getExternalApisUser() {
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + "getUser");

        responseInfo = externalService.getExternalAllUser();

        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }

    @PostMapping("/external/importuser")
    public Object importExternalApisUser() {
        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                reqRawText + "importUserFromExternalApi");

        responseInfo = externalService.importExternalAllUser();

        logService.saveLogInfo(new Object() {}.getClass().getEnclosingMethod().getName(),
                resRawText + responseInfo);
        return responseInfo;
    }
}
