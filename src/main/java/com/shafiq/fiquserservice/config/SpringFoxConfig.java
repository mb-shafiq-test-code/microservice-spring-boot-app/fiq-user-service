package com.shafiq.fiquserservice.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Bean
    public Docket myApp() {

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Shafiq: User API")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.shafiq.fiquserservice"))
                .paths(Predicates.or(
                        PathSelectors.regex("/user/.*")
                ))
                .build()
                .apiInfo(apiInfo("Shafiq: User API", "User Transaction API reference for developers"))
                ;
    }

    @Bean
    public Docket myApp2() {

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Shafiq: External API")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.shafiq.fiquserservice"))
                .paths(Predicates.or(
                        PathSelectors.regex("/external/.*")
                ))
                .build()
                .apiInfo(apiInfo("Shafiq: External API", "External API reference for developers"))
                ;
    }

    private ApiInfo apiInfo(String title, String desc) {
        return new ApiInfoBuilder().title(title)
                .description(desc)
                .termsOfServiceUrl("")
                .contact(new Contact("Shafiq Alias","","shafiq.alias91@gmail.com"))
                .license("Shafiq Alias 2021")
                .licenseUrl("").version("1.0").build();
    }
}
