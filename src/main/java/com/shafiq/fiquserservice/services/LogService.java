package com.shafiq.fiquserservice.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
public class LogService {

    private final static String LOG_PATH = System.getProperty("user.dir") + File.separator + "saved_logs";

    private void storeToTextFile(String input) {
        String LOG_FILE = "%s-hourly_logs.txt";
        String logFile = String.format(LOG_FILE, new SimpleDateFormat("yyyyMMdd-HH").format(new Date()));
        try {
            File directory = new File(LOG_PATH);
            File file = new File(LOG_PATH + File.separator + logFile);
            if (!directory.exists()) {
                directory.mkdir();
            }

            BufferedWriter fw = new BufferedWriter(new FileWriter(file.getAbsoluteFile(), true));

            fw.write(input);
            fw.newLine();

            fw.flush();
            fw.close();
        } catch (IOException ex) {
            log.error("[" + (new Date()).toString() + "][Logger is experiencing troubles {}]", ex.getMessage());
        }
    }

    public void saveLogInfo(String className, String input) {
        String log = "[INFO][" + (new Date()).toString() + "][" + className + "] ";
        log += input;

        storeToTextFile(log);
    }

    public void saveLogError(String className, String input) {
        String log = "[ERROR][" + (new Date()).toString() + "][" + className + "] ";
        log += input;

        storeToTextFile(log);
    }
}
