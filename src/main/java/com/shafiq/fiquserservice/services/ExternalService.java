package com.shafiq.fiquserservice.services;

import com.shafiq.fiquserservice.controller.response.ExtUserResponse;
import com.shafiq.fiquserservice.controller.response.ResponseInfo;
import com.shafiq.fiquserservice.services.dto.AccountDTO;
import com.shafiq.fiquserservice.services.externalapis.ExternalAPISOne;
import com.shafiq.fiquserservice.services.masterdata.MasterDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ExternalService {
    @Autowired
    ExternalAPISOne externalAPISOne;

    @Autowired
    MasterDataService masterDataService;

    public ResponseInfo getExternalAllPost(){
        try{
            List<Object> response =  Optional.ofNullable(externalAPISOne.getAllPosts()).orElse(null);

            return ResponseInfo.builder()
                    .responseStatus(Objects.isNull(response) ? "F":"S")
                    .responseCode(Objects.isNull(response) ? "202":"000")
                    .responseMessage(Objects.isNull(response) ? "List is null":"List of posts retrieved")
                    .responseDetail(Optional.ofNullable(response).orElse(null))
                    .build();
        } catch (Exception e){
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("201")
                    .responseMessage(e.getClass().getName())
                    .responseDetail(e.getMessage())
                    .build();
        }

    }

    public ResponseInfo getExternalAllUser(){
        try{
            List<ExtUserResponse> response =  Optional.ofNullable(externalAPISOne.getAllUser()).orElse(null);

            return ResponseInfo.builder()
                    .responseStatus(Objects.isNull(response) ? "F":"S")
                    .responseCode(Objects.isNull(response) ? "202":"000")
                    .responseMessage(Objects.isNull(response) ? "List is null":"List of users retrieved")
                    .responseDetail(Optional.ofNullable(response).orElse(null))
                    .build();
        } catch (Exception e){
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("201")
                    .responseMessage(e.getClass().getName())
                    .responseDetail(e.getMessage())
                    .build();
        }

    }
    public ResponseInfo importExternalAllUser(){
        try{
            List<ExtUserResponse> importList =  Optional.ofNullable(externalAPISOne.getAllUser()).orElse(null);
            Object response = null;
            if(!Objects.isNull(importList)){
                log.info("importList size before filter {}",importList.size());
                importList = importList.stream()
                        .filter(u -> Objects.isNull(masterDataService.findEmail(u.getEmail())))
                        .collect(Collectors.toList());
                log.info("importList size after filter {}",importList.size());
                if(importList.size() == 0){
                    return ResponseInfo.builder()
                            .responseStatus("S")
                            .responseCode("000")
                            .responseMessage("All user has been imported previously")
                            .responseDetail(null)
                            .build();
                }


                List<AccountDTO> listAcc = new ArrayList<>();
                for(ExtUserResponse ex : importList){

                    AccountDTO acc = AccountDTO.builder()
                            .email(ex.getEmail())
                            .fullName(ex.getName())
                            .mobileNo(ex.getPhone())
                            .birthDate(getRandomDate())
                            .task(getRandomTask())
                            .build();

                    listAcc.add(acc);
                }

                response = masterDataService.addListAccount(listAcc);
            }

            return ResponseInfo.builder()
                    .responseStatus(Objects.isNull(response) ? "F":"S")
                    .responseCode(Objects.isNull(response) ? "202":"000")
                    .responseMessage(Objects.isNull(response) ? "List is null":"List of users retrieved")
                    .responseDetail(Optional.ofNullable(response).orElse(null))
                    .build();
        } catch (Exception e){
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("201")
                    .responseMessage(e.getClass().getName())
                    .responseDetail(e.getMessage())
                    .build();
        }
    }

    public static Date getRandomDate(){

        LocalDate start = LocalDate.of(1970, Month.JANUARY, 01);
        LocalDate end = LocalDate.of(1999, Month.DECEMBER, 31);
        long randomDay = ThreadLocalRandom
                .current()
                .nextLong(start.toEpochDay(), end.toEpochDay());

        Instant instant = LocalDate.ofEpochDay(randomDay).atStartOfDay(ZoneId.systemDefault()).toInstant();

        return Date.from(instant);
    }

    public static String getRandomTask(){
        List<String> givenList = Arrays.asList("Admin","Clerk","Staff");
        Random rand = new Random();
        return givenList.get(rand.nextInt(givenList.size()));
    }

}
