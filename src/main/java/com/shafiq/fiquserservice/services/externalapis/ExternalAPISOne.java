package com.shafiq.fiquserservice.services.externalapis;



import com.shafiq.fiquserservice.controller.response.ExtUserResponse;
import feign.Logger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;


@FeignClient(name = "dummy-apis-one", url="https://jsonplaceholder.typicode.com", configuration = ExternalAPISOne.Conf.class)
public interface ExternalAPISOne {

    @Slf4j
    public class Conf {
        @Bean
        public Logger.Level feignLoggerLevel() {
            if (log.isDebugEnabled()) {
                return Logger.Level.FULL;
            } else {
                return Logger.Level.NONE;
            }
        }
    }

    @GetMapping("/posts")
    List<Object> getAllPosts();

    @GetMapping("/users")
    List<ExtUserResponse> getAllUser();
}
