package com.shafiq.fiquserservice.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shafiq.fiquserservice.controller.request.AccountRequest;
import com.shafiq.fiquserservice.controller.request.PageAccountRequest;
import com.shafiq.fiquserservice.controller.response.ResponseInfo;
import com.shafiq.fiquserservice.services.dto.AccountDTO;
import com.shafiq.fiquserservice.services.masterdata.MasterDataService;
import com.shafiq.fiquserservice.utils.BaseUtil;
import com.shafiq.fiquserservice.controller.request.DeleteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    MasterDataService masterDataService;


    public ResponseInfo saveUserAccount(AccountRequest request) {

        if (Objects.equals("", BaseUtil.getStr(request.getEmail()))) {
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("101")
                    .responseMessage("Email must not empty!")
                    .build();
        }

        AccountDTO user = masterDataService.addAccount(AccountDTO.builder()
                .email(request.getEmail())
                .fullName(request.getFullName())
                .birthDate(request.getBirthDate())
                .mobileNo(request.getMobileNo())
                .task(request.getTask())
                .build());

        return ResponseInfo.builder()
                .responseStatus(Optional.ofNullable(user).map(a -> "S").orElse("F"))
                .responseCode(Optional.ofNullable(user).map(a -> "000").orElse("102"))
                .responseMessage(Optional.ofNullable(user).map(a -> "Success").orElse("Fail"))
                .responseDetail(Optional.ofNullable(user).orElse(null))
                .build();
    }

    public ResponseInfo updateUserAccount(AccountRequest request) {

        if (Objects.equals("", BaseUtil.getStr(request.getEmail()))) {
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("101")
                    .responseMessage("Email must not empty!")
                    .build();
        }

        AccountDTO user = masterDataService.editAccount(AccountDTO.builder()
                .email(request.getEmail())
                .fullName(request.getFullName())
                .birthDate(request.getBirthDate())
                .mobileNo(request.getMobileNo())
                .task(request.getTask())
                .build());

        return ResponseInfo.builder()
                .responseStatus(Optional.ofNullable(user).map(a -> "S").orElse("F"))
                .responseCode(Optional.ofNullable(user).map(a -> "000").orElse("102"))
                .responseMessage(Optional.ofNullable(user).map(a -> "Success").orElse("Fail or user not found / has been deleted"))
                .responseDetail(Optional.ofNullable(user).orElse(null))
                .build();
    }


    public ResponseInfo deleteUserAccount(DeleteRequest request, boolean isHardDelete) {

        if (Objects.equals("", BaseUtil.getStr(request.getEmail()))) {
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("101")
                    .responseMessage("Email must not empty!")
                    .build();
        }
        DeleteRequest acc = DeleteRequest.builder()
                .email(request.getEmail())
                .build();

        boolean status = masterDataService.removeAccount(acc,isHardDelete);

        return ResponseInfo.builder()
                .responseStatus(status ? "S" : "F")
                .responseCode(status ? "000" : "102")
                .responseMessage(status ? "Success" : "Fail")
                .responseDetail((isHardDelete ? "Hard" : "Soft") + " Deleting email is " + (status ? "OK" : "failed due to not exist"))
                .build();
    }

    public ResponseInfo readUserAccount(String email) {

        if (Objects.equals("", BaseUtil.getStr(email))) {
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode("101")
                    .responseMessage("Email must not empty!")
                    .build();
        }

        AccountDTO user = masterDataService.findEmail(email);

        return ResponseInfo.builder()
                .responseStatus(Optional.ofNullable(user).map(a -> "S").orElse("F"))
                .responseCode(Optional.ofNullable(user).map(a -> "000").orElse("103"))
                .responseMessage(Optional.ofNullable(user).map(a -> "Success").orElse("User Not Found"))
                .responseDetail(Optional.ofNullable(user).orElse(null))
                .build();
    }

    public ResponseInfo getUserAccountList(PageAccountRequest request) {
        Integer page = BaseUtil.getInt(request.getPage());
        Integer size = BaseUtil.getInt(request.getSize()) == 0 ? 10 : BaseUtil.getInt(request.getSize());

        request.setPage(page);
        request.setSize(size);

        ObjectMapper o = new ObjectMapper();
        Map<String, Object> map = o.convertValue(request, Map.class);

        Map<String, Object> mapObj = masterDataService.getPagelist(map);

        return ResponseInfo.builder()
                .responseStatus(Optional.of(BaseUtil.getInt(mapObj.get("totalElement")) > 0).map(a -> "S").orElse("F"))
                .responseCode(Optional.of(BaseUtil.getInt(mapObj.get("totalElement")) > 0).map(a -> "000").orElse("104"))
                .responseMessage(Optional.of(BaseUtil.getInt(mapObj.get("totalElement")) > 0).map(a -> "Success").orElse("No available user account lists"))
                .responseDetail(Optional.of(BaseUtil.getInt(mapObj.get("totalElement")) > 0).map(a -> mapObj).orElse(null))
                .build();
    }

}
