package com.shafiq.fiquserservice.services.masterdata;



import com.shafiq.fiquserservice.controller.request.DeleteRequest;
import com.shafiq.fiquserservice.controller.request.PageAccountRequest;
import com.shafiq.fiquserservice.services.dto.AccountDTO;
import feign.Logger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@FeignClient(name = "fiq-master-data-service", configuration = MasterDataService.Conf.class)
public interface MasterDataService {

    @Slf4j
    public class Conf {
        @Bean
        public Logger.Level feignLoggerLevel() {
            if (log.isDebugEnabled()) {
                return Logger.Level.FULL;
            } else {
                return Logger.Level.NONE;
            }
        }
    }

    @PostMapping("/data/internal/account/add")
    AccountDTO addAccount(@RequestBody AccountDTO request);

    @PostMapping("/data/internal/account/addList")
    Object addListAccount(@RequestBody List<AccountDTO> request);

    @PostMapping("/data/internal/account/update")
    AccountDTO editAccount(@RequestBody AccountDTO accountDTO);

    @PostMapping("/data/internal/account/delete")
    boolean removeAccount(@RequestBody DeleteRequest deleteRequest, @RequestParam(defaultValue = "false") boolean hardDelete);

    @GetMapping("/data/internal/account/email")
    AccountDTO findEmail(@RequestParam String email);

    @GetMapping("/data/internal/account/list/page")
    Map<String, Object> getPagelist(@RequestParam  Map<String, Object> params);
}
