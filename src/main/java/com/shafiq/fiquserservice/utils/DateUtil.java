package com.shafiq.fiquserservice.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

public class DateUtil {

    public static final ZoneId TIME_ZONE = ZoneId.of("Asia/Kuala_Lumpur");
    public static final String EWALLET_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss+ZZ";
    private final static DateTimeFormatter FORMATTER =
            new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd'T'HH:mm:ss")
                    .toFormatter()
                    .withZone(TIME_ZONE);
    private final static DateTimeFormatter TIME_FORMATTER =
            new DateTimeFormatterBuilder().appendPattern("HHmm")
                    .toFormatter();
    private final static DateTimeFormatter DATE_FORMATTER =
            new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd")
                    .toFormatter();
    private final static DateTimeFormatter formatter =
            new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
                    .appendOffsetId()
                    .toFormatter();
    private final static DateTimeFormatter formatterNoMillis =
            new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd'T'HH:mm:ss")
                    .appendOffsetId()
                    .toFormatter();

    private static final DateTimeFormatter FORMATTER_EWALLET = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
            .appendOffsetId()
            .toFormatter();

    public static Instant parseDateEwallet(String text) {
        if (text == null) {
            return null;
        }
        return Instant.from(FORMATTER_EWALLET.parse(text));
    }

    public static int getDayOfWeek(Instant instant) {
        return instant.atZone(TIME_ZONE).getDayOfWeek().getValue();
    }

    public static Instant parse(String dateStr) {
        if (dateStr == null)
            return null;
        return Instant.from(FORMATTER.parse(dateStr));
    }

    public static LocalDate parseDate(String dateStr) {
        if (dateStr == null)
            return null;
        return LocalDate.from(DATE_FORMATTER.parse(dateStr));
    }

    public static LocalTime parseTime(String dateStr) {
        if (dateStr == null)
            return null;
        return LocalTime.from(TIME_FORMATTER.parse(dateStr));
    }

    public static String format(Instant instant) {
        if (instant == null)
            return null;
        return FORMATTER.format(instant);
    }

    public static String formatTime(LocalTime time) {
        if (time == null)
            return null;
        return TIME_FORMATTER.format(time);
    }

    public static long getUnixTimestamp() {
        return Instant.now().getEpochSecond();
    }

    public static String getSignature(String signature) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = md.digest(signature.getBytes(StandardCharsets.UTF_8));

        return bytesToHex(encodedhash);
    }

    public static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static Instant parse(String dateStr, String pattern) {
        if (pattern.equalsIgnoreCase("")) {
            pattern = "yyyy-MM-dd'T'HH:mm:ss";
        }
        DateTimeFormatter df =
                new DateTimeFormatterBuilder().appendPattern(pattern)
                        .toFormatter()
                        .withZone(TIME_ZONE);
        if (dateStr == null)
            return null;
        return Instant.from(df.parse(dateStr));
    }

    /**
     * Check if the given String is ISO 8601 Date Format
     *
     * @param dateStr
     * @return
     */
    public static Boolean isISO8601(String dateStr) {
        return parseISO8601(dateStr) != null;
    }

    public static Boolean isISO8601WithoutMillis(String dateStr) {
        return parseISO8601WithoutMillis(dateStr) != null;
    }

    /**
     * Parse the given String from ISO 8601 format to Date object
     *
     * @param dateStr
     * @return
     */
    public static Date parseISO8601(String dateStr) {
        if (dateStr == null)
            return null;

        try {
            OffsetDateTime offsetDateTime = OffsetDateTime.parse(dateStr);

            if (offsetDateTime != null) {
                return Date.from(offsetDateTime.toInstant());
            }
        } catch (java.time.format.DateTimeParseException ex) {
            return null;
        }

        return null;
    }

    public static Instant parseISO8601WithoutMillis(String dateStr) {
        if (dateStr == null)
            return null;
        return Instant.from(formatterNoMillis.parse(dateStr));
    }


    /**
     * Convert Date to LocalDateTime
     *
     * @param dateToConvert date to convert
     * @return
     */
    public static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    /**
     * Convert Date to Local Date
     *
     * @param dateToConvert date to convert
     * @return
     */
    public static LocalDate convertToLocalDate(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    /**
     * Format the Date object to ISO 8601 Date String
     *
     * @param date
     * @return
     */
    public static String formatISO8601(Date date) {
        if (date == null)
            return null;

        return OffsetDateTime.ofInstant(date.toInstant(), TIME_ZONE)
                .toZonedDateTime()
                .format(formatter);
    }

    public static String formatISO8601(Instant instant) {
        if (instant == null)
            return null;

        return formatter.format(instant.atZone(TIME_ZONE));
    }

    public static String formatISO8601WithoutMillis(Instant instant) {
        return formatterNoMillis.format(instant.atZone(TIME_ZONE));
    }

    /**
     * Get current Date in ISO 8601 Date String
     *
     * @return
     */
    public static String nowISO8601() {
        return OffsetDateTime.ofInstant(Instant.now(), TIME_ZONE)
                .toZonedDateTime()
                .format(formatter);
    }

    public static String nowISO8601NoMillis() {
        return OffsetDateTime.ofInstant(Instant.now(), TIME_ZONE)
                .toZonedDateTime()
                .format(formatterNoMillis);
    }

    /**
     * Get only date (yyyy-MM-dd) from ISO 8601 Date String
     *
     * @param dateStr
     * @return
     */
    public static String extractDateFromISO8601(String dateStr) {
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(dateStr);
        if (offsetDateTime != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return simpleDateFormat.format(Date.from(offsetDateTime.toInstant()));
        }

        return null;
    }

    /**
     * Get current Date with Simple Date Format
     *
     * @return
     */
    public static String now() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return sdf.format(new Date());
    }

    /**
     * Get the String format with Pattern given
     *
     * @param pattern the date format pattern, e.g. yyyyMMddHHmm
     * @param date    the date object
     * @return the date in String with pattern format or null if failed to format
     */
    public static String getFormatWithPattern(String pattern, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * @param source
     * @param destination
     * @return
     */
    public static Duration compareDateTime(Date source, Date destination) {
        LocalDateTime sourceTime = convertToLocalDateTime(source);
        LocalDateTime destinationTime = convertToLocalDateTime(destination);
        return Duration.between(sourceTime, destinationTime);
    }

    /**
     * @param source
     * @param destination
     * @return
     */
    public static Period compareDate(Date source, Date destination) {
        LocalDate sourceDate = convertToLocalDate(source);
        LocalDate destinationDate = convertToLocalDate(destination);
        return Period.between(sourceDate, destinationDate);
    }

    public static Instant toInstant(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.toInstant();
        }
    }

    public static Duration compareTimeDifference(Instant startInclusive, Instant endExclusive){
        if(startInclusive == null){
            startInclusive = Instant.now();
        }
        if(endExclusive == null){
            endExclusive = Instant.now();
        }
        return Duration.between(startInclusive, endExclusive);
    }


}
